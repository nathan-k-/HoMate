# HoMate
Assistant pour la maison !

Permet d'avoir la température et le taux d'humidité dans un beau graphique.

## Stack matériel 

- Raspberry Pi V1
- ESP8266
- DHT22

## Stack logiciel

- Grafana
- InfluxDB (Docker rpi)
- NodeRed (Docker rpi)
- Mosquitto (Docker rpi)
- Arduino IDE pour programmer l'ESP
